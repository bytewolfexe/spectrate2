{
	description = "spectrate - a simple image viewer program";

	inputs = {
		nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
	};

	outputs = {
		self,
		nixpkgs,
		...
	}@extra:
	let
		system = "x86_64-linux";						# Not tested on other platforms!
		pkgs = import nixpkgs { inherit system; }; 
	in
	{
		packages.${system}.default = pkgs.stdenv.mkDerivation {
			name = "spectrate";
			version = "0.0.1";

			src = ./.;

			doCheck = false;

			buildInputs = with pkgs; [ freeimage SDL2 ];

			buildPhase = ''
			make spectrate
			'';

			installPhase = ''
			mkdir -p $out/bin
			cp spectrate $out/bin/spectrate
			'';
		};
	};
}
