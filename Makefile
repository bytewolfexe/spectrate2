CPPFLAGS=-Wall -Wextra -Werror -std=c++20 -I include/
LDFLAGS=-lfreeimage -lSDL2

.PHONY: clean

main.o:
	$(CXX) -c $(CFLAGS) $(CPPFLAGS) src/main.cpp -o $@

pixel.o:
	$(CXX) -c $(CFLAGS) $(CPPFLAGS) src/pixel.cpp -o $@

image.o:
	$(CXX) -c $(CFLAGS) $(CPPFLAGS) src/image.cpp -o $@

spectrate: main.o image.o pixel.o
	$(CXX) main.o image.o pixel.o -o -l $(LDFLAGS) -o $@

clean:
	rm -rf main.o image.o pixel.o spectrate
