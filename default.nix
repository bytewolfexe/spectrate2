{ pkgs ? import <nixpkgs> {} }: pkgs.stdenv.mkDerivation {
	name = "spectrate";

	version = "1.0.0";

	src = ./.;

	buildInputs = with pkgs; [ freeimage SDL2 ];

	doCheck = false;

	buildPhase = ''
	make spectrate
	'';

	installPhase = ''
	mkdir -p $out/bin
	cp spectrate $out/bin/spectrate
	'';
}
