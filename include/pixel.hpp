#ifndef SPECTRATE_PIXEL_HPP
#define SPECTRATE_PIXEL_HPP


#include <cstdint>


namespace spectrate {
	struct Pixel {
		std::uint8_t data[4];

		std::uint8_t &red() noexcept;
		std::uint8_t &green() noexcept;
		std::uint8_t &blue() noexcept;
		std::uint8_t &alpha() noexcept;
	};
}

#endif // SPECTRATE_PIXEL_HPP
