#ifndef SPECTRATE_IMAGE_HPP
#define SPECTRATE_IMAGE_HPP


#include <cstdint>
#include <filesystem>
#include <FreeImage.h>
#include "pixel.hpp"


namespace spectrate {
	namespace fs = std::filesystem;

	class Image {
	public:
		Image(const fs::path &path) noexcept;

		Image(Image &&image) noexcept;

		~Image() noexcept;

		bool is_ready() const noexcept;

		std::uint32_t get_width() const noexcept;

		std::uint32_t get_height() const noexcept;

		Pixel get_pixel(std::uint32_t x, std::uint32_t y) const noexcept;

		void set_pixel(std::uint32_t x, std::uint32_t y, Pixel p) noexcept;

		Pixel average() const noexcept;

		Pixel average_area(double x, double y, double width, double height) const noexcept;

	private:
		bool m_ready;

		std::uint32_t m_width, m_height;
		FIBITMAP *m_image;
	};
}


#endif // SPECTRATE_IMAGE_HPP
