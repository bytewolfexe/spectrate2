#include "image.hpp"

#include <iostream>

using spectrate::Image, spectrate::Pixel;

namespace spectrate {
	Image::Image(const fs::path &path) noexcept
		: m_ready{false}, m_width{0}, m_height{0}, m_image{nullptr}
	{
		auto file = path.string();

		FREE_IMAGE_FORMAT format = FreeImage_GetFIFFromFilename(file.c_str());

		FIBITMAP *img = FreeImage_Load(format, file.c_str(), 0);
		if(!img) {
			return;
		}

		// Convertion to 32 bit (4 byte) image.
		FIBITMAP *conv = FreeImage_ConvertTo32Bits(img);

		// img is not needed anymore.
		FreeImage_Unload(img);

		if(!conv) {
			return;
		}

		FreeImage_FlipVertical(conv);

		std::uint32_t width = FreeImage_GetWidth(conv), height = FreeImage_GetHeight(conv);


		m_image = conv;

		m_width = width;
		m_height = height;

		m_ready = true;
	}


	Image::Image(Image &&image) noexcept {
		m_ready = image.m_ready;

		m_width = image.m_width;
		m_height = image.m_height;

		m_image = image.m_image;

		image.m_ready = false;
		image.m_width = 0;
		image.m_height = 0;
		image.m_image = nullptr;
	}


	Image::~Image() noexcept {
		FreeImage_Unload(m_image);
	}


	bool Image::is_ready() const noexcept {
		return m_ready;
	}


	std::uint32_t Image::get_width() const noexcept {
		return m_width;
	}


	std::uint32_t Image::get_height() const noexcept {
		return m_height;
	}


	Pixel Image::get_pixel(std::uint32_t x, std::uint32_t y) const noexcept {
		if(!m_ready) {
			return Pixel { 0, 0, 0, 0 };
		}
		if(x >= m_width || y >= m_height) {
			return Pixel { 0, 0, 0, 0 };
		}

		Pixel *pixels = (Pixel*)FreeImage_GetBits(m_image);

		return pixels[y * m_width + x];
	}


	void Image::set_pixel(std::uint32_t x, std::uint32_t y, Pixel p) noexcept {
		if(!m_ready) {
			return;
		}
		if(x >= m_width || y >= m_height) {
			return;
		}

		Pixel *pixels = (Pixel*)FreeImage_GetBits(m_image);

		pixels[y * m_width + x] = p;
	}

	Pixel Image::average() const noexcept {
		if(!m_ready) {
			return Pixel{0, 0, 0, 0};
		}
		std::size_t r = 0, g = 0, b = 0, a = 0, sum = 0;

		auto *pixels = (Pixel*)FreeImage_GetBits(m_image);

		for(std::size_t i = 0; i < m_width * m_height; i++) {
			Pixel p = pixels[i];

			r += p.data[FI_RGBA_RED];
			g += p.data[FI_RGBA_GREEN];
			b += p.data[FI_RGBA_BLUE];
			a += p.data[FI_RGBA_ALPHA];

			sum += 1;
		}

		if(sum == 0) {
			return Pixel{0, 0, 0, 0};
		}

		auto result = Pixel{};

		result.data[FI_RGBA_RED] = (std::uint8_t)(r / sum);
		result.data[FI_RGBA_GREEN] = (std::uint8_t)(g / sum);
		result.data[FI_RGBA_BLUE] = (std::uint8_t)(b / sum);
		result.data[FI_RGBA_ALPHA] = (std::uint8_t)(a / sum);

		return result;
	}


	Pixel Image::average_area(double x, double y, double width, double height) const noexcept {
		if(!m_ready) {
			return Pixel{0, 0, 0, 0};
		}

		std::size_t r = 0, g = 0, b = 0, a = 0, sum = 0;

		auto *pixels = (Pixel*)FreeImage_GetBits(m_image);


		if(x < 0 || x + width >= m_width || y < 0 || y + height >= m_height) {
			return Pixel{0,0,0,0};
		}

		for(std::int32_t j = y; j < y + height; j++) {
			for(std::int32_t i = x; i < x + width; i++) {
				Pixel p = pixels[j * m_width + i];

				r += p.data[FI_RGBA_RED];
				g += p.data[FI_RGBA_GREEN];
				b += p.data[FI_RGBA_BLUE];
				a += p.data[FI_RGBA_ALPHA];

				sum += 1;
			}
		}

		auto result = Pixel{};

		result.data[FI_RGBA_RED] = (std::uint8_t)(r / sum);
		result.data[FI_RGBA_GREEN] = (std::uint8_t)(g / sum);
		result.data[FI_RGBA_BLUE] = (std::uint8_t)(b / sum);
		result.data[FI_RGBA_ALPHA] = (std::uint8_t)(a / sum);

		return result;
	}
}
