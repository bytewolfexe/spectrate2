#include "pixel.hpp"

#include <FreeImage.h>


namespace spectrate {
	std::uint8_t &Pixel::red() noexcept {
		return data[FI_RGBA_RED];
	}

	std::uint8_t &Pixel::green() noexcept {
		return data[FI_RGBA_GREEN];
	}

	std::uint8_t &Pixel::blue() noexcept {
		return data[FI_RGBA_BLUE];
	}

	std::uint8_t &Pixel::alpha() noexcept {
		return data[FI_RGBA_ALPHA];
	}
}
