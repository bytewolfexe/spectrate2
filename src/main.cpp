#include <filesystem>
#include <FreeImage.h>
#include <iostream>
#include <SDL2/SDL.h>
#include <string>
#include <vector>

#include "pixel.hpp"
#include "image.hpp"


using spectrate::Image, spectrate::Pixel;

namespace fs = std::filesystem;

constexpr auto PERM_SKIP = fs::directory_options::skip_permission_denied;


// Checks if given path represents a supported image file.
bool is_valid_image(const fs::path &path) noexcept;


template <typename Iter>
void collect_images(Iter &it, std::vector<Image> &out_images) noexcept;


void calculate_render_dimensions(
		double window_width, double window_height,
		const Image &image, double image_scale,
		double &out_x, double &out_y,
		double &out_width, double &out_height) noexcept;


void parse_arguments(const std::vector<std::string> &args, std::vector<Image> &out_images) noexcept;


// TODO add window boundaries checking
void render_image(SDL_Renderer *renderer, const Image &image,
		double window_width, double window_height,
		double offset_x, double offset_y,
		double render_width, double render_height) noexcept;

bool init_sdl_window(const char *title,
		std::uint32_t x, std::uint32_t y, 
		std::uint32_t width, std::uint32_t height,
		SDL_Window **out_window, SDL_Renderer **out_renderer) noexcept;


int main(int argc, char **argv) {
	auto args = std::vector<std::string>{};

	SDL_Window *window = nullptr;
	SDL_Renderer *renderer = nullptr;

	std::vector<Image> images;

	std::size_t current_image = 0;

	bool running = true;

	auto e = SDL_Event{};

	double render_width = 0.0, render_height = 0.0;
	double image_scale = 0.8;


	for(int i = 1; i < argc; i++) {
		auto s = std::string{argv[i]};
		args.emplace_back(std::move(s));
	}

	parse_arguments(args, images);

	if(args.empty()) {
		std::cerr << "No file path arguments specified!\r\n";
		return 1;
	}

	FreeImage_Initialise();

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cerr << "Failed to initialize SDL!\r\n";
		return 1;
	}

	std::uint32_t window_x = SDL_WINDOWPOS_CENTERED;
	std::uint32_t window_y = SDL_WINDOWPOS_CENTERED;
	std::uint32_t window_width = 720;
	std::uint32_t window_height = 480;


	if(!init_sdl_window("spectrate", window_x, window_y, window_width, window_height, &window, &renderer)) {
		std::cerr << "Failed to initialize window!\r\n";
		return 1;
	}

	if(images.empty()) {
		std::cerr << "No images were provided or loaded!\r\n";
		return 1;
	}

	std::cout << "Loaded " << images.size() << " images!\r\n";

	double offset_x = 16.0, offset_y = 16.0;

	double user_offset_x = 0.0, user_offset_y = 0.0;

	double offset_scalar = 10.0;

	
	auto redraw = [&]() {
		calculate_render_dimensions(
			window_width,
			window_height,
			images.at(current_image),
			image_scale,
			offset_x,
			offset_y,
			render_width,
			render_height
		); 

		render_image(
			renderer,
			images.at(current_image),
			window_width, window_height,
			offset_x + user_offset_x, offset_y + user_offset_y,
			render_width, render_height
		); 
	};

	redraw();

	while(running) {
		while(SDL_PollEvent(&e)) {
			switch(e.type) {
			case SDL_QUIT:
				running = false;
				break;
			case SDL_KEYUP:
				{
					int key = e.key.keysym.sym;

					if(key == SDLK_q) {
						running = false;
					}
					else if(key == SDLK_h) {
						// Previous image
						if(current_image == 0) {
							current_image = images.size() - 1;
						}
						else {
							current_image -= 1;
						}
						image_scale = 0.8;
						user_offset_x = 0.0;
						user_offset_y = 0.0;
						redraw();
					}
					else if(key == SDLK_l) {
						// Next image
						if(current_image == images.size() - 1) {
							current_image = 0;
						}
						else {
							current_image += 1;
						}
						image_scale = 0.8;
						user_offset_x = 0.0;
						user_offset_y = 0.0;
						redraw();
					}
					else if(key == SDLK_j) {
						image_scale -= 0.1;

						if(image_scale < 0.1) {
							image_scale = 0.1;
						}
						redraw();
					}
					else if(key == SDLK_k) {
						image_scale += 0.1;

						if(image_scale > 4.0) {
							image_scale = 4.0;
						}
						redraw();
					}
					else if(key == SDLK_w) {
						user_offset_y += offset_scalar * image_scale;
						redraw();
					}
					else if(key == SDLK_s) {
						user_offset_y -= offset_scalar * image_scale;
						redraw();
					}
					else if(key == SDLK_d) {
						user_offset_x -= offset_scalar * image_scale;
						redraw();
					}
					else if(key == SDLK_a) {
						user_offset_x += offset_scalar * image_scale;
						redraw();
					}

				}
				break;
			case SDL_WINDOWEVENT:
				{
					switch(e.window.event) {
						case SDL_WINDOWEVENT_RESIZED:
						case SDL_WINDOWEVENT_SIZE_CHANGED:
							{
								// TODO change render dimensions
								window_width = e.window.data1;
								window_height = e.window.data2;

								image_scale = 0.8;

								redraw();
							}
							break;
						default:
							break;
					}
				}
				break;
			default:
				break;
			}
		}
	}

	FreeImage_DeInitialise();

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}




bool is_valid_image(const fs::path &path) noexcept {
	if(!fs::exists(path)) {
		return false;
	}
	if(!fs::is_regular_file(path)) {
		return false;
	}
	if(!path.has_extension()) {
		return false;
	}
	auto ext = path.extension().string();

	// Converting each character in string to lowercase.
	for(auto &c : ext) {
		c |= 32;
	}

	bool result =
		ext == ".png" ||
		ext == ".jpg" ||
		ext == ".jpeg" ||
		ext == ".webm" ||
		ext == ".webp";

	return result;
}


template <typename Iter>
void collect_images(Iter &it, std::vector<Image> &out_images) noexcept {
	for(const auto &entry : it) {
		if(!is_valid_image(entry)) {
			continue;
		}
		auto image = Image(entry);

		if(!image.is_ready()) {
			std::cerr << "Failed to load image " << entry << "!\r\n";
			continue;
		}

		out_images.emplace_back(std::move(image));
	}
}


void render_image(SDL_Renderer *renderer,
		const Image &image,
		double window_width, double window_height,
		double offset_x, double offset_y,
		double render_width, double render_height) noexcept {
	auto p = image.average();

	SDL_SetRenderDrawColor(renderer, p.red(), p.green(), p.blue(), 255);
	SDL_RenderClear(renderer);

	double step_x = (double)image.get_width() / render_width;
	double step_y = (double)image.get_height() / render_height;

	for(double i = 0.0; i < render_width; i += 1.0) {
		for(double j = 0.0; j < render_height; j += 1.0) {
			if(j + offset_y >= window_height) {
				continue;
			}
			Pixel p = image.average_area(i * step_x, j * step_y, step_x, step_y);

			SDL_SetRenderDrawColor(renderer, p.data[FI_RGBA_RED], p.data[FI_RGBA_GREEN], p.data[FI_RGBA_BLUE], 255);

			SDL_RenderDrawPoint(renderer, (int)(i + offset_x), (int)(j + offset_y));
		}
		if(i + offset_x >= window_width) {
			continue;
		}
	}

	SDL_RenderPresent(renderer);
}


void parse_arguments(const std::vector<std::string> &args, std::vector<Image> &out_images) noexcept {
	bool recursive = false;

	for(auto &arg : args) {
		// Enable recursion.
		if(arg == "-r") {
			recursive = true;
		}
		else if(fs::is_directory(arg) && fs::exists(arg)) {
			if(recursive) {
				auto dir_iter = fs::recursive_directory_iterator{arg, PERM_SKIP};
				collect_images(dir_iter, out_images);
			}
			else {
				auto dir_iter = fs::directory_iterator{arg, PERM_SKIP};
				collect_images(dir_iter, out_images);
			}
		}
		else if(is_valid_image(arg)) {
			auto image = Image{arg};
			if(!image.is_ready()) {
				std::cerr << "Failed to load image " << arg << "!\r\n";
				continue;
			}
			out_images.emplace_back(std::move(image));
		}
	}
}


bool init_sdl_window(const char *title,
		std::uint32_t x, std::uint32_t y,
		std::uint32_t width, std::uint32_t height,
		SDL_Window **out_window, SDL_Renderer **out_renderer) noexcept {
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		return false;
	}

	SDL_Window *window = SDL_CreateWindow(title, x, y, width, height, SDL_WINDOW_RESIZABLE);

	if(!window) {
		SDL_Quit();
		return false;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);

	if(!renderer) {
		SDL_DestroyWindow(window);
		SDL_Quit();
		return false;
	}

	*out_window = window;
	*out_renderer = renderer;

	return true;
}


void calculate_render_dimensions(
		double window_width, double window_height,
		const Image &image,
		double image_scale, 
		double &out_x, double &out_y,
		double &out_width, double &out_height) noexcept {
	double image_width = (double)image.get_width();
	double image_height = (double)image.get_height();

	double image_ratio = image_height / image_width;

	if(image_width >= image_height) {
		out_width = window_width * image_scale;
		out_height = out_width * image_ratio;
	}
	else {
		out_height = window_height * image_scale;
		out_width = out_height / image_ratio;
	}

	out_x = (window_width - out_width) / 2.0;
	out_y = (window_height - out_height) / 2.0;
}
