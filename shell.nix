{ pkgs ? import <nixpkgs> {} }: pkgs.mkShell {
	buildInputs = with pkgs; [ freeimage SDL2 ];
	nativeBuildInputs = with pkgs; [ gdb valgrind bear ];
}
